package libvirt

import (
  "errors"
	"fmt"
	"log"
	"net"
	"os/exec"
	"time"

	"github.com/digitalocean/go-libvirt"
)

type LibvirtHandler struct {
	lv *libvirt.Libvirt
	sshCommand *exec.Cmd
}

func runSshCommand(username string, server string, sshKeyFile string) *exec.Cmd {
	log.Printf("Starting ssh!")
	cmd := exec.Command("ssh", "-i",sshKeyFile,fmt.Sprintf("%s@%s", username, server), "-N","-L", "*:5000:/var/run/libvirt/libvirt-sock")
  log.Printf("Args: %v", cmd.Args)
	go func() {
		output, err := cmd.Output()
		if err != nil {
      if err.Error() != "signal: killed" {
        log.Printf("error running ssh: %v", err)
      }
		}
    log.Printf("Output:")
		log.Printf(string(output))
	}()
	return cmd
}

func DomainStateToString(state int32) string {
	switch libvirt.DomainState(state) {
	case libvirt.DomainNostate:
		return "No State"
	case libvirt.DomainRunning:
		return "Running"
	case libvirt.DomainBlocked:
		return "Blocked"
	case libvirt.DomainPaused:
		return "Paused"
	case libvirt.DomainShutdown:
		return "Shutdown"
	case libvirt.DomainShutoff:
		return "Shutoff"
	case libvirt.DomainCrashed:
		return "Crashed"
	case libvirt.DomainPmsuspended:
		return "Suspended"
	}
	return "Unkown state"
}

func New(username string, server string, sshKeyFile string) (*LibvirtHandler, error) {
	sshCommand := runSshCommand(username, server, sshKeyFile)
	var err error
	var c net.Conn
  log.Printf("Connection to localhost:5000")
	for tries := 1; tries <= 10; tries = tries + 1 {
		time.Sleep(time.Millisecond * 200)
    log.Printf("attempt %d of %d", tries, 10)
		c, err = net.DialTimeout("tcp", "localhost:5000", 2*time.Second)
		if err == nil {
			break
		}
	}
	if err != nil {
		log.Printf("failed to dial libvirt: %v", err)
		return nil, err
	}
	l := libvirt.New(c)
	if err := l.Connect(); err != nil {
		log.Fatalf("failed to connect: %v", err)
	}
	v, err := l.ConnectGetLibVersion()
	if err != nil {
		log.Fatalf("failed to retrieve libvirt version: %v", err)
	}
	fmt.Println("Version:", v)
	return &LibvirtHandler{
		l,
		sshCommand,
	}, nil
}

func (h *LibvirtHandler) Disconnect() {
	if err := h.lv.Disconnect(); err != nil {
		log.Fatalf("failed to disconnect: %v", err)
	}
	h.sshCommand.Process.Kill()
}

func (h* LibvirtHandler) ListVMs() ([]libvirt.Domain, error) {
	flags := libvirt.ConnectListDomainsActive | libvirt.ConnectListDomainsInactive
	domains, _, err := h.lv.ConnectListAllDomains(1, flags)
	if err != nil {
		return nil, err
	}
	return domains, nil
}

func (h* LibvirtHandler) GetVMStatus(vm libvirt.Domain) (int32, error) {
	rState, _, err := h.lv.DomainGetState(vm, 0)
	if err != nil {
		return 0, err
	}
	return rState, nil
}

func (h* LibvirtHandler) StartVM(vm libvirt.Domain) error {
	state, err := h.GetVMStatus(vm)
	if err != nil {
		return err
	}
	switch libvirt.DomainState(state) {
	case libvirt.DomainNostate: fallthrough
	case libvirt.DomainShutdown: fallthrough
	case libvirt.DomainShutoff: fallthrough
	case libvirt.DomainCrashed: fallthrough
	case libvirt.DomainBlocked:
		err = h.lv.DomainCreate(vm)
		break;
	case libvirt.DomainRunning:
		break;
	case libvirt.DomainPaused: fallthrough
	case libvirt.DomainPmsuspended:
		err = h.lv.DomainResume(vm)
		break;
	}
	if err != nil {
		return err
	}
	return nil
}

func (h* LibvirtHandler) PauseVM(vm libvirt.Domain) error {
	state, err := h.GetVMStatus(vm)
	if err != nil {
		return err
	}
	switch libvirt.DomainState(state) {
	case libvirt.DomainRunning:
		err = h.lv.DomainSuspend(vm)
		break;
	case libvirt.DomainNostate: fallthrough
	case libvirt.DomainShutdown: fallthrough
	case libvirt.DomainShutoff: fallthrough
	case libvirt.DomainCrashed: fallthrough
	case libvirt.DomainBlocked: fallthrough
	case libvirt.DomainPaused: fallthrough
	case libvirt.DomainPmsuspended:
    err = errors.New("vm is not in a pausable state")
		break;
	}
	if err != nil {
		return err
	}
	return nil
}
