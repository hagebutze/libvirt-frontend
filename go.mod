module libvirt-frontend

go 1.15

require (
    github.com/gorilla/securecookie v1.1.1
	github.com/digitalocean/go-libvirt v0.0.0-20201213030128-2cef0d5c9a34
	github.com/gorilla/sessions v1.2.1
	github.com/thanhpk/randstr v1.0.4
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
