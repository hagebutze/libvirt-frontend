package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/thanhpk/randstr"
	"golang.org/x/oauth2"
	"io/ioutil"
	config2 "libvirt-frontend/config"
	"libvirt-frontend/http_handler"
	"log"
	"net/http"
	"os"
	"strings"
)

var authConfig *oauth2.Config
var oauthStateString = randstr.Hex(16)
var oauthUserinfoEndpoint = ""
var cookieStore = sessions.NewCookieStore([]byte(securecookie.GenerateRandomKey(32)))

func main() {
	// Load config
	if os.Getenv("CONFIG") == "" {
		log.Fatalf("Plase set CONFIG environment variable!")
	}
	log.Printf("Opening config file %s\n", os.Getenv("CONFIG"))
	bytes, err := ioutil.ReadFile(os.Getenv("CONFIG"))
	if err != nil {
		log.Fatalf("error loading config: %v", err)
	}
	config := config2.Config{}
	err = json.Unmarshal(bytes, &config)
	if err != nil {
		log.Fatalf("error loading config: %v", err)
	}
	// Allow overwriting ssh key file
	if os.Getenv("OVERWRITE_SSH_KEY_FILE_PATH") != "" {
		config.SSHKeyFile = os.Getenv("OVERWRITE_SSH_KEY_FILE_PATH")
	}

	// Load the secret for oauth2 files
	oauthClientId, err := os.ReadFile(config.OAuthClientIdFile)
	if err != nil {
		log.Fatal("Cannot open auth client id file " + config.OAuthClientIdFile + ": " + err.Error())
	}
	oauthSecret, err := os.ReadFile(config.OAuthSecretFile)
	if err != nil {
		log.Fatal("Cannot open auth secret file " + config.OAuthSecretFile + ": " + err.Error())
	}
	oauthAuthEndpoint, err := os.ReadFile(config.OAuthAuthEndpointFile)
	if err != nil {
		log.Fatal("Cannot open auth endpoint file " + config.OAuthAuthEndpointFile + ": " + err.Error())
	}
	_oauthUserinfoEndpoint, err := os.ReadFile(config.OAuthUserinfoEndpointFile)
	if err != nil {
		log.Fatal("Cannot open userinfo endpoint file" + config.OAuthUserinfoEndpointFile + ": " + err.Error())
	}
	oauthUserinfoEndpoint = strings.Trim(string(_oauthUserinfoEndpoint), " \n\t")
	oauthTokenEndpoint, err := os.ReadFile(config.OAuthTokenEndpointFile)
	if err != nil {
		log.Fatal("Cannot open token endpoint file " + config.OAuthTokenEndpointFile + ": " + err.Error())
	}
	// Initialize oauth2
	authConfig = &oauth2.Config{
		RedirectURL:  config.RedirectURL,
		ClientID:     strings.Trim(string(oauthClientId), " \n\t"),
		ClientSecret: strings.Trim(string(oauthSecret), " \n\t"),
		Scopes:       []string{"openid"},
		Endpoint: oauth2.Endpoint{
			AuthURL:   strings.Trim(string(oauthAuthEndpoint), " \n\t"),
			TokenURL:  strings.Trim(string(oauthTokenEndpoint), " \n\t"),
			AuthStyle: oauth2.AuthStyleInHeader,
		},
	}

	//Handling the requests
	http.HandleFunc("/",
		func(res http.ResponseWriter, req *http.Request) {
			if verifyLogin(res, req) {
				http_handler.StatusHandler(config, res, req)
			}
		})
	http.HandleFunc("/start",
		func(res http.ResponseWriter, req *http.Request) {
			if verifyLogin(res, req) {
				http_handler.StartHandler(config, res, req)
			}
		})
	http.HandleFunc("/pause",
		func(res http.ResponseWriter, req *http.Request) {
			if verifyLogin(res, req) {
				http_handler.PauseHandler(config, res, req)
			}
		})

	log.Fatal(http.ListenAndServe(":8080", nil))
}

func verifyAccessTokenAndPermissions(accessToken string, res http.ResponseWriter, req *http.Request) bool {
	url := oauthUserinfoEndpoint
	var bearer = "Bearer " + accessToken
	oauthReq, err := http.NewRequest("GET", url, nil)
	oauthReq.Header.Add("Authorization", bearer)
	client := &http.Client{}
	response, err := client.Do(oauthReq)
	if err != nil {
		res.WriteHeader(403)
		log.Printf("error logging in: %s\n", err.Error())
		fmt.Fprintf(res, "failed getting user info, cannot authorize you: %s", err.Error())
		return false
	}
	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		res.WriteHeader(403)
		log.Printf("error logging in during reading of response body: %s\n", err.Error())
		fmt.Fprintf(res, "failed getting user info, cannot authorize you: %s", err.Error())
		return false
	}
	// Now check the roles
	var objmap map[string]json.RawMessage
	err = json.Unmarshal(contents, &objmap)
	if err != nil {
		res.WriteHeader(403)
		log.Printf("error logging in: %s\nReponse is not json: %s\n", err.Error(), string(contents))
		fmt.Fprintf(res, "failed reading plain response body: %s", err.Error())
		return false
	}
	var roles []string
	err = json.Unmarshal(objmap["roles"], &roles)
	if err != nil {
		res.WriteHeader(403)
		log.Printf("error logging in: %s\nReponse roles is not json: %s\n", err.Error(), string(contents))
		fmt.Fprintf(res, "failed reading roles in response body: %s", err.Error())
		return false
	}
	// Check if any of the needs roles are in the list
	neededRoleFound := false
	for _, role := range roles {
		if role == "finanzen" || role == "admin" {
			neededRoleFound = true
		}
	}
	if !neededRoleFound {
		res.WriteHeader(403)
		fmt.Fprint(res, "sorry, but you are not authorized to anything here")
		return false
	}
	return true
}

/**
 * Verifies, that the user is logged in.
 *
 * If the user is not logged in, sends an appropriate response. And returns false.
 * Otherwise return true.
 */
func verifyLogin(res http.ResponseWriter, req *http.Request) bool {
	// First check if we already have a session cookie
	session, _ := cookieStore.Get(req, "libvirt-controller-session")
	if access_token, ok := session.Values["access-token"]; ok {
		// We have an access token!
		result := verifyAccessTokenAndPermissions(access_token.(string), res, req)
		if result {
			return true
		}
	}
	// We have no access token
	stateString, codeString := req.FormValue("state"), req.FormValue("code")
	if stateString != oauthStateString {
		url := authConfig.AuthCodeURL(oauthStateString)
		http.Redirect(res, req, url, http.StatusTemporaryRedirect)
		return false
	}
	token, err := authConfig.Exchange(oauth2.NoContext, codeString)
	if err != nil {
		res.WriteHeader(403)
		fmt.Fprintf(res, "code exchange failed, cannot authorize you: %s", err.Error())
		return false
	}
	session.Values["access-token"] = token.AccessToken
	err = session.Save(req, res)
	if err != nil {
		http.Error(res, err.Error(), http.StatusInternalServerError)
		return false
	}
	// Redirect with cookie
	http.Redirect(res, req, authConfig.RedirectURL, http.StatusTemporaryRedirect)
	return false
}
