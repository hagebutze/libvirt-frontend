package http_handler

import (
	"fmt"
	"libvirt-frontend/config"
	"libvirt-frontend/libvirt"
	"log"
	"net/http"
)

type Action = string;

const (
  START_ACTION Action="start";
  PAUSE_ACTION Action="pause";
)

/** Generic action handler on a VM.
*
* Since start/resume/pause ... actions do almost all the same but doing the action,
* these handler use this general helper function.
*/
func ActionHandler(action Action, config config.Config ,res http.ResponseWriter, req *http.Request) {
  log.Printf("Serving start handler")
	// Check correct method
	if req.Method != "POST" {
    log.Printf("Got %s method != POST on start", req.Method)
		res.WriteHeader(404)
		return
	}

  err := req.ParseForm()
  if err != nil {
		res.WriteHeader(400)
		res.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
  }

  vm_name := req.Form.Get("vm_name")
  if vm_name == "" {
		res.WriteHeader(400)
		res.Write([]byte("vm_name not provided"))
		return
  }

	handler, err := libvirt.New(config.Username, config.Sever, config.SSHKeyFile)
	if err != nil {
		res.WriteHeader(503)
		res.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	defer handler.Disconnect()
	vms, err := handler.ListVMs()
	if err != nil {
		res.WriteHeader(503)
		res.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	for _, vm := range vms {
		if vm.Name == vm_name {
      var err error;
      switch action {
      case START_ACTION:
        err = handler.StartVM(vm)
        break;
      case PAUSE_ACTION:
        err = handler.PauseVM(vm)
        break;
      }
			if err != nil {
				res.WriteHeader(503)
				res.Write([]byte(fmt.Sprintf("error: %v", err)))
				return
			}
			res.WriteHeader(200)
			res.Write([]byte("success!"))
      return
		}
	}
  res.WriteHeader(400)
  res.Write([]byte("vm not found!"))
  return
}
