package http_handler

import (
	"fmt"
  "log"
	"libvirt-frontend/config"
	"libvirt-frontend/libvirt"
	"net/http"
)

func StatusHandler(config config.Config, res http.ResponseWriter, req *http.Request) {
  log.Printf("Serving status handler")
	// Check basic request parameters
	if req.Method != "GET" {
		res.WriteHeader(404)
		return
	}

	handler, err := libvirt.New(config.Username, config.Sever, config.SSHKeyFile)
	if err != nil {
		res.WriteHeader(503)
		res.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	defer handler.Disconnect()
	vms, err := handler.ListVMs()
	if err != nil {
		res.WriteHeader(503)
		res.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}

  // Generate the positive Response
  res.WriteHeader(200)
  res.Write([]byte(
  fmt.Sprintf(
    "<!DOCTYPE html>\n" +
    "<html>\n" +
    "<body>\n\n" +
    "<h1>Butzenbuero Status</h1>\n\n",
      )))

	for _, vm := range vms {
    res.Write([]byte(
      fmt.Sprintf(
        "<p>" +
        vm.Name +
        ": ",
        )))
    status, err := handler.GetVMStatus(vm)
    if err != nil {
      res.Write([]byte(fmt.Sprintf("error: %v", err)))
    }
    res.Write([]byte(
      fmt.Sprintf(
        libvirt.DomainStateToString(status) +
        "</p>" +
        "<form action=\"./start\" method=\"POST\">" +
        fmt.Sprintf("<input type=\"hidden\" id=\"vm_name\" name=\"vm_name\" value=\"%s\">", vm.Name) +
        "<input type=\"submit\" value=\"Starten\">" +
        "</form>" +
        "<form action=\"./pause\" method=\"POST\">" +
        fmt.Sprintf("<input type=\"hidden\" id=\"vm_name\" name=\"vm_name\" value=\"%s\">", vm.Name) +
        "<input type=\"submit\" value=\"Pausieren\">" +
        "</form>",
        ),
      ),
    )
	}
}
