
package http_handler

import (
	"libvirt-frontend/config"
	"net/http"
)

func PauseHandler(config config.Config ,res http.ResponseWriter, req *http.Request) {
  ActionHandler(START_ACTION, config, res, req);
}
