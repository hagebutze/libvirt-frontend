package config

type Config struct {
	RedirectURL               string `json:"url"`
	Username                  string `json:"username"`
	Sever                     string `json:"server"`
	SSHKeyFile                string `json:"ssh_key_file"`
	OAuthHost                 string `json:"oauth_host"`
	OAuthClientIdFile         string `json:"oauth_client_id_file"`
	OAuthSecretFile           string `json:"oauth_secret_file"`
	OAuthAuthEndpointFile     string `json:"oauth_auth_endpoint_file"`
	OAuthTokenEndpointFile    string `json:"oauth_token_endpoint_file"`
	OAuthUserinfoEndpointFile string `json:"oauth_userinfo_endpoint_file"`
}

